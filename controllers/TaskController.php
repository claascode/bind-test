<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use app\models\Task;

class TaskController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'ajax-delete' => ['post'],
                    'ajax-set-status' => ['post']
                ],
            ],
        ];
    }
    
    // View tasks list
    public function actionIndex($sort = null)
    {
        switch($sort)
        {
            case 'incomplete':
                $query = Task::find()->where("status = 0");
                break;
            case 'complete':
                $query = Task::find()->where("status = 1");
                break;
            default:
                $query = Task::find();
                break;
        }
        
        $dataProvider   = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 25,
            ],
        ]);
        
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    // View task
    public function actionView($id)
    {
        $model = $this->find($id);
        
        return $this->render('view', [
            'model' => $model
        ]);
    }

    // Create new task
    public function actionCreate()
    {
        $model = new Task();
        
        // Create logic
        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            // Creating successful
            if ($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Task created!'));
                return $this->refresh();
            }
            
            // Creating failed
            else
            {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Something went wrong!'));
                return $this->refresh();
            }
        }
        
        return $this->render('create', [
            'model' => $model
        ]);
    }
    
    // Update existing task
    public function actionUpdate($id)
    {
        $model = $this->find($id);
        
        // Update logic
        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            // Update successful
            if ($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Task updated!'));
                return $this->refresh();
            }
            
            // Update failed
            else
            {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Something went wrong!'));
                return $this->refresh();
            }
        }
        
        return $this->render('update', [
            'model' => $model
        ]);
    }
    
    // Ajax update task
    public function actionAjaxUpdate($id)
    {
        // Find existing model
        $model = $this->find($id);
        // Populate model with new values
        $model->load(Yii::$app->request->post());
        
        // Return update result as boolean
        return $model->save();
    }

    // Delete task (post request only)
    public function actionDelete($id)
    {
        // Find model to delete
        $model = $this->find($id);
        
        // Deletion success
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Task deleted!'));
            return $this->redirect(['index']);
        }
        
        // Deletion failed
        else
        {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Task deletion failed!'));
            return $this->redirect(['index']);
        }
    }
    
    // Delete task via ajax request
    public function actionAjaxDelete($id)
    {
        // Find model to delete
        $model = $this->find($id);
        
        // Return deletion result (boolean)
        return $model->delete();
    }
    
    // Set task status via ajax request
    public function actionAjaxSetStatus($id, $status)
    {
        $model = $this->find($id);
        
        if ($status == '1' || $status == '0')
        {
            $model->status = $status;
            return $model->save();
        }
        
        else
        {
            return false;
        }
            
    }

    // Find task model via ar model
    private function find($id)
    {
        $model = Task::findOne($id);
        
        if ($model !== null) 
        {
            return $model;
        }
        else 
        {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
