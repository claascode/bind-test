-- -----------------------------------------------------
-- Schema bind
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `bind` ;
CREATE SCHEMA IF NOT EXISTS `bind` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `bind` ;

-- -----------------------------------------------------
-- Table `task`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `task` ;

CREATE TABLE IF NOT EXISTS `task` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `due_date` DATETIME NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;