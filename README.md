INSTALLATION
--------------------------------------------------------------------------------
1. install vendor libraries with "composer install" command in root directory
2. configure config/db.php
3. install database from db/install.sql