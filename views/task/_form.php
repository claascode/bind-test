<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php 
$action = ($model->isNewRecord) ? '' : Yii::$app->urlManager->createUrl(['task/ajax-update', 'id' => $model->id]);
$form = ActiveForm::begin([
    'id' => 'task-form',
    'action' => $action,
    'method' => 'post'
]); 
?>

    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'description')->textarea() ?>
    <?= $form->field($model, 'due_date')->textInput() ?>
    <?=
    $form->field($model, 'status')->dropDownList([
        '0' => \Yii::t('app', 'Undone'),
        '1' => \Yii::t('app', 'Done')
    ]);
    ?>


    <div class="form-group clearfix">
        <?= 
        Html::submitButton(
            Yii::t('app', 'Save'), 
            [
                'class' => $model->isNewRecord ? 'btn btn-success save-btn' : 'btn btn-primary save-btn'
            ],
            [
                'class' => 'pull-left'
            ]
        ) 
        ?>
        
        <?php if (!$model->isNewRecord): ?>
        <?= 
        Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) 
        ?>
        <?php endif; ?>
    </div>

<?php ActiveForm::end(); ?>