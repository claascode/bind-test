<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app', 'Tasks')
?>

<?= $this->render('_header'); ?>

<h2><?= $this->title ?></h2>
<?php \yii\widgets\Pjax::begin(); ?>
<?php
echo GridView::widget([
    'id' => 'task-list',
    'dataProvider'=> $dataProvider,
    'summary' => false,
    'columns' => [
        [
            'attribute' => false,
            'format' => 'raw',
            'value' => function ($model) 
            {                
                return 
                    Html::a(($model->status == 1) 
                            ? '<span class="glyphicon glyphicon-remove"></span>' 
                            : '<span class="glyphicon glyphicon-ok"></span>',
                        [
                            'ajax-set-status',
                            'id' => $model->id,
                            'status' => ($model->status == 1) ? 0 : 1
                        ],
                        [
                            'class' => 'set-status'
                        ]
                    );

            },
        ],
        'title',
        'due_date',
        [
            'attribute' => false,
            'format' => 'raw',
            'value' => function ($model) 
            {                
                return 
                    Html::a(Yii::t('app', 'View'),
                        [
                            'view',
                            'id' => $model->id,
                        ]
                    );

            },
        ],
        [
            'attribute' => false,
            'format' => 'raw',
            'value' => function ($model) 
            {                
                return 
                    Html::a(Yii::t('app', 'Delete'),
                        [
                            'ajax-delete',
                            'id' => $model->id,
                        ],
                        [
                            'class' => 'delete'
                        ]
                    );

            },
        ]
    ]
]);
?>
<?php \yii\widgets\Pjax::end(); ?>

<?php $this->registerJs(\Yii::$app->controller->renderPartial('js/index.js')); ?>