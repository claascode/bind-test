<?php
use yii\helpers\Html;

$this->title = Yii::t('app', 'Task')
?>

<?= $this->render('_header'); ?>

<h2><?= $this->title ?></h2>
<table class="table table-striped table-bordered">
    <tbody>
        <tr>
            <th><?= Yii::t('app', 'Title') ?></th>
            <td><?= Html::encode($model->title) ?></td>
        </tr>
        
        <tr>
            <th><?= Yii::t('app', 'Description') ?></th>
            <td><?= Html::encode($model->description) ?></td>
        </tr>
        
        <tr>
            <th><?= Yii::t('app', 'Due date') ?></th>
            <td><?= Html::encode($model->due_date) ?></td>
        </tr>
        
        <tr>
            <th><?= Yii::t('app', 'Status') ?></th>
            <td><?= ($model->status === 1) ? Yii::t('app', 'Done') : Yii::t('app', 'Undone') ?></td>
        </tr>
    </tbody>
</table>

<div class="clearfix">
    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary pull-left']) ?>
    <?= 
    Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger pull-right',
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) 
    ?>
</div>