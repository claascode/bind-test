<?php
$this->title = Yii::t('app', 'Add new task');
?>

<?= $this->render('_header'); ?>

<h2><?= $this->title ?></h2>
<?= $this->render('_form', ['model' => $model]); ?>