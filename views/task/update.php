<?php
$this->title = Yii::t('app', 'Edit task');
?>

<?= $this->render('_header'); ?>

<h2><?= $this->title ?></h2>
<?= $this->render('_form', ['model' => $model]); ?>
<?php $this->registerJs(\Yii::$app->controller->renderPartial('js/update.js')); ?>