$( document ).ready(function() {
    // Delete event
    $( "a.delete" ).click(function( event ) {
        event.preventDefault();
        deleteTask($(this));
    });
    
    // Update task status
    $( "a.set-status" ).click(function( event ) {
        event.preventDefault();
        updateTaskStatus($(this));
    });
    
    // Delete function
    function deleteTask(trigger)
    {
        confirm("Are you sure you want to delete this item?");
        
        $.ajax({
            url: trigger.prop('href'),
            method: 'post'
        })
            .always(function(data){
                // Delete successful
                if (data == '1')
                {
                    $.pjax.defaults.timeout = false;
                    $.pjax.reload({container: '#task-list', timeout: 2000});
                }
                // Delete failed
                else
                {
                    var message = 'Something went wrong!';
                    var type = 'danger';
                    setMessage(message, type);
                }
        });
    };
    
    // Update task status function
    function updateTaskStatus(trigger)
    {   
        $.ajax({
            url: trigger.prop('href'),
            method: 'post'
        })
            .always(function(data){
                // Delete successful
                if (data == '1')
                {
                   $.pjax.defaults.timeout = false;
                   $.pjax.reload({container: '#task-list', timeout: 2000});
                }
                // Delete failed
                else
                {
                    var message = 'Something went wrong!';
                    var type = 'danger';
                    setMessage(message, type);
                }
        });
    };
    
    /*
     * Displays a message string
     * types: success, danger, warning, info
     */
    function setMessage(message, type)
    {
        $('.message-area').html('<div class="alert alert-'+ type +'" role="alert">'+ message +'</div>');
    };
    // Set error message
    
});