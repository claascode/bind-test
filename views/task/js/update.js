$( document ).ready(function() {
    // Delete event
    $( "button.save-btn" ).click(function( event ) {
        event.preventDefault();
        
        updateTask($(this).closest('form'));
    });
    
    // Update function
    function updateTask(form)
    {   
        $.ajax({
            url: form.prop('action'),
            data: form.serialize(),
            method: 'post'
        })
            .always(function(data){
                // Update successful
                if (data == '1')
                {
                    var message = 'Task updated!';
                    var type = 'success';
                }
                // Update failed
                else
                {
                    var message = 'Something went wrong!';
                    var type = 'danger';
                }
                
            setMessage(message, type);
        });
    };
    
    /*
     * @string message
     * @types: success, danger, warning, info
     */
    function setMessage(message, type)
    {
        $('.message-area').html('<div class="alert alert-'+ type +'" role="alert">'+ message +'</div>');
    };
    // Set error message
    
});