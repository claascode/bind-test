<?php
use yii\helpers\Html;
?>

<header class="clearfix">
    <div class="pull-left">
        <?= Html::a(Yii::t('app', 'Add new task'), ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    
    <div class="pull-right">
        <?= Html::a(Yii::t('app', 'Completed tasks'), ['index', 'sort' => 'complete'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('app', 'Incomplete tasks'), ['index', 'sort' => 'incomplete'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('app', 'All tasks'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
</header>

<hr />